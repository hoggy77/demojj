package com.demo.johnson.repositories;

import com.demo.johnson.model.AddDeviceModel;
import com.demo.johnson.model.DeviceInfo;
import com.demo.johnson.model.DeviceUpdateRequest;
import com.demo.johnson.network.NetworkProvider;

import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class DeviceRepository {

    private static DeviceRepository INSTANCE = null;

    public static DeviceRepository getInstance() {

        if(INSTANCE == null){
            INSTANCE = new DeviceRepository();
        }
        return INSTANCE;
    }

    public Single<List<DeviceInfo>> getDeviceList(){
        return NetworkProvider.getAPIService().getDevices();
    }

    public Single<ResponseBody> updatedevice(String deviceID, DeviceUpdateRequest deviceUpdateRequest){
        return NetworkProvider.getAPIService().updateDevice(deviceID, deviceUpdateRequest);
    }

    public Single<AddDeviceModel> addDevice(AddDeviceModel addDeviceModel){
        return NetworkProvider.getAPIService().addDevice(addDeviceModel);
    }

    public Call<ResponseBody> deleteDevice(String deviceID){
        return NetworkProvider.getAPIService().deleteDevice(deviceID);
    }

}
