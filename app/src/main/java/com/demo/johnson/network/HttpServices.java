package com.demo.johnson.network;

import android.bluetooth.BluetoothClass;

import com.demo.johnson.model.AddDeviceModel;
import com.demo.johnson.model.DeviceInfo;
import com.demo.johnson.model.DeviceUpdateRequest;

import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface HttpServices {

    @GET("/devices")
    Single<List<DeviceInfo>> getDevices();

    @POST("/devices")
    Single<AddDeviceModel> addDevice(@Body AddDeviceModel addDeviceModel);

    @POST("/devices/{device_id}")
    Single<ResponseBody> updateDevice(@Path("device_id") String deviceID, @Body DeviceUpdateRequest deviceUpdateRequest);

    @DELETE("/devices/{device_id}")
    Call<ResponseBody> deleteDevice(@Path("device_id") String deviceID);

}
