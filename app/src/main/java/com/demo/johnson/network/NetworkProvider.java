package com.demo.johnson.network;

public class NetworkProvider {

    public static HttpServices getAPIService() {

        return RetrofitClient.getClient().create(HttpServices.class);
    }
}
