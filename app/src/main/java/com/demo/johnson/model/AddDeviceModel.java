package com.demo.johnson.model;

import com.google.gson.annotations.SerializedName;

public class AddDeviceModel {



    @SerializedName("device")
    private String device;
    @SerializedName("os")
    private String os;
    @SerializedName("manufacturer")
    private String manufacturer;

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
}
