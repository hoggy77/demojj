package com.demo.johnson.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class DeviceInfo implements Parcelable {

    @SerializedName("id")
    private int id;
    @SerializedName("device")
    private String device;
    @SerializedName("os")
    private String os;
    @SerializedName("manufacturer")
    private String manufacturer;
    @SerializedName("lastCheckedOutDate")
    private String lastCheckedOutDate;
    @SerializedName("lastCheckedOutBy")
    private String lastCheckedOutBy;
    @SerializedName("isCheckedOut")
    private boolean isCheckedOut;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getLastCheckedOutDate() {
        return lastCheckedOutDate;
    }

    public void setLastCheckedOutDate(String lastCheckedOutDate) {
        this.lastCheckedOutDate = lastCheckedOutDate;
    }

    public String getLastCheckedOutBy() {
        return lastCheckedOutBy;
    }

    public void setLastCheckedOutBy(String lastCheckedOutBy) {
        this.lastCheckedOutBy = lastCheckedOutBy;
    }

    public boolean isCheckedOut() {
        return isCheckedOut;
    }

    public void setCheckedOut(boolean checkedOut) {
        isCheckedOut = checkedOut;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.device);
        dest.writeString(this.os);
        dest.writeString(this.manufacturer);
        dest.writeString(this.lastCheckedOutDate);
        dest.writeString(this.lastCheckedOutBy);
        dest.writeByte(this.isCheckedOut ? (byte) 1 : (byte) 0);
    }

    public DeviceInfo() {
    }

    protected DeviceInfo(Parcel in) {
        this.id = in.readInt();
        this.device = in.readString();
        this.os = in.readString();
        this.manufacturer = in.readString();
        this.lastCheckedOutDate = in.readString();
        this.lastCheckedOutBy = in.readString();
        this.isCheckedOut = in.readByte() != 0;
    }

    public static final Parcelable.Creator<DeviceInfo> CREATOR = new Parcelable.Creator<DeviceInfo>() {
        @Override
        public DeviceInfo createFromParcel(Parcel source) {
            return new DeviceInfo(source);
        }

        @Override
        public DeviceInfo[] newArray(int size) {
            return new DeviceInfo[size];
        }
    };
}
