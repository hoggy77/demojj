package com.demo.johnson.model;

import com.google.gson.annotations.SerializedName;

public class DeviceUpdateRequest {


           @SerializedName("lastCheckedOutDate")
           private String lastCheckedOutDate;
           @SerializedName("lastCheckedOutBy")
           private String lastCheckedOutBy;
           @SerializedName("isCheckedOut")
           private boolean isCheckedOut;

    public String getLastCheckedOutDate() {
        return lastCheckedOutDate;
    }

    public void setLastCheckedOutDate(String lastCheckedOutDate) {
        this.lastCheckedOutDate = lastCheckedOutDate;
    }

    public String getLastCheckedOutBy() {
        return lastCheckedOutBy;
    }

    public void setLastCheckedOutBy(String lastCheckedOutBy) {
        this.lastCheckedOutBy = lastCheckedOutBy;
    }

    public boolean isCheckedOut() {
        return isCheckedOut;
    }

    public void setCheckedOut(boolean checkedOut) {
        isCheckedOut = checkedOut;
    }
}
