package com.demo.johnson.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.demo.johnson.R;
import com.demo.johnson.ui.CustomItemTouchHelperCallback;
import com.demo.johnson.model.DeviceInfo;
import com.demo.johnson.ui.DetailDeviceActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DeviceAdapter extends RecyclerView.Adapter<DeviceAdapter.VH> implements CustomItemTouchHelperCallback.TouchHelperInterface {

    private List<DeviceInfo> mData = new ArrayList();
    private Context mContext;
    private CustomItemTouchHelperCallback.TouchHelperInterface activityListener;

    public DeviceAdapter(CustomItemTouchHelperCallback.TouchHelperInterface activityListener){
        this.activityListener = activityListener;
    }

    public void updateData(List<DeviceInfo> list){
       this.mData = list;
       notifyDataSetChanged();

    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        mContext = viewGroup.getContext();
        return new VH(LayoutInflater.from(mContext).inflate(R.layout.view_device_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH vh, int position) {
        vh.onBind(mData.get(position));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public boolean onItemMove(int startPosition, int endPosition) {

        if (startPosition < endPosition) {
            for (int i = startPosition; i < endPosition; i++) {
                Collections.swap(mData, i, i + 1);
            }
        } else {
            for (int i = startPosition; i > endPosition; i--) {
                Collections.swap(mData, i, i - 1);
            }
        }
        notifyItemMoved(startPosition, endPosition);
        return true;
    }

    @Override
    public void onItemDismiss(int adapterPosition) {

        mData.remove(adapterPosition);
        notifyItemRemoved(adapterPosition);
        activityListener.onItemDismiss(adapterPosition);
    }

    public DeviceInfo getItem(int position) {
        return mData.get(position);
    }

    public class VH extends RecyclerView.ViewHolder{

        private TextView firstRow;
        private TextView secondRow;
        private LinearLayout rootView;

        public VH(@NonNull View itemView) {
            super(itemView);
            rootView = itemView.findViewById(R.id.rootView);
            firstRow = itemView.findViewById(R.id.firstRow);
            secondRow = itemView.findViewById(R.id.secondRow);

        }

        public void onBind(final DeviceInfo item){
            firstRow.setText(item.getDevice() + " - " + item.getOs());
            if(item.getLastCheckedOutBy() != null) {
                secondRow.setText("Checked Out by" + " " + item.getLastCheckedOutBy());
            }else{
                secondRow.setText(mContext.getString(R.string.device_checkout_available));
            }

            rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, DetailDeviceActivity.class);
                    intent.putExtra(DetailDeviceActivity.DEVICE, item);
                    mContext.startActivity(intent);
                }
            });
        }
    }


}
