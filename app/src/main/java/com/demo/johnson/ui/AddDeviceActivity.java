package com.demo.johnson.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.demo.johnson.R;
import com.demo.johnson.model.AddDeviceModel;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class AddDeviceActivity extends AppCompatActivity {

    private EditText deviceNameEdit, osEdit, manufacturerEdit ;
    private DeviceViewModel deviceViewModel;
    private CompositeDisposable dispoasable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_device);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        init();
    }

    private void init(){

        deviceViewModel = ViewModelProviders.of(AddDeviceActivity.this).get(DeviceViewModel.class);
        deviceNameEdit = findViewById(R.id.device_name);
        osEdit = findViewById(R.id.os_name);
        manufacturerEdit = findViewById(R.id.manufacturer_name);
    }

    private void checkFieldsAndUpdate() {

        if(!deviceNameEdit.getText().toString().equalsIgnoreCase("") &&
                !osEdit.getText().toString().equalsIgnoreCase("") &&
                !manufacturerEdit.getText().toString().equalsIgnoreCase("")){

            AddDeviceModel addDeviceModel = new AddDeviceModel();
            addDeviceModel.setDevice(deviceNameEdit.getText().toString());
            addDeviceModel.setOs(osEdit.getText().toString());
            addDeviceModel.setManufacturer(manufacturerEdit.getText().toString());
            saveDevice(addDeviceModel);

        }
    }

    private void saveDevice(AddDeviceModel addDeviceModel){

        dispoasable.add(deviceViewModel.addDevice(addDeviceModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<AddDeviceModel>() {
                    @Override
                    public void accept(AddDeviceModel addDeviceModel) throws Exception {
                        finish();
                    }
                }));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_items_layout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;

            case R.id.save_btn:
                checkFieldsAndUpdate();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dispoasable.dispose();
    }
}
