package com.demo.johnson.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.demo.johnson.R;
import com.demo.johnson.model.DeviceInfo;
import com.demo.johnson.model.DeviceUpdateRequest;
import com.demo.johnson.utils.DateUtil;
import com.demo.johnson.utils.DialogUtil;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

public class DetailDeviceActivity extends AppCompatActivity {

    public static String DEVICE = "device";
    private final String TYPE_UPDATE = "type_update";
    private final String TYPE_INFO = "type_info";
    private DeviceViewModel deviceViewModel;
    private CompositeDisposable disposable = new CompositeDisposable();
    private TextView btnStatus, deviceName, osName, manufacturer, lastUserCheckedOut, lastDateCheckedOut;
    private DeviceInfo deviceInfoGlobal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_device);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        init();
    }

    private void init(){

        deviceName = findViewById(R.id.device);
        osName = findViewById(R.id.os);
        manufacturer = findViewById(R.id.manufacturer);
        lastUserCheckedOut = findViewById(R.id.last_user_checked_out);
        lastDateCheckedOut = findViewById(R.id.last_date_checked_out);
        btnStatus = findViewById(R.id.btn_change_state);
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            deviceInfoGlobal = bundle.getParcelable(DEVICE);
            if(deviceInfoGlobal != null) {
                fillInfo();
            }
        }
        deviceViewModel = ViewModelProviders.of(DetailDeviceActivity.this).get(DeviceViewModel.class);

        btnStatus = findViewById(R.id.btn_change_state);
        btnStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(deviceInfoGlobal.isCheckedOut()){
                    updateDevice(getUpdateRequest(true, null));

                }else {
                   //showDialog(TYPE_UPDATE);
                    DialogUtil.showDialog(DetailDeviceActivity.this, DialogUtil.TYPE_UPDATE, null, new DialogUtil.DialogClickListener() {
                        @Override
                        public void onClick(String name) {
                            updateDevice(getUpdateRequest(true, name));

                        }
                    });


                }
            }
        });

    }

    private void fillInfo(){

        deviceName.setText(getString(R.string.detail_device_name) + deviceInfoGlobal.getDevice());
        osName.setText(getString(R.string.detail_os_name) + deviceInfoGlobal.getOs());
        manufacturer.setText(getString(R.string.detail_manufacturer_name) + deviceInfoGlobal.getManufacturer());
        if(deviceInfoGlobal.getLastCheckedOutBy() != null) {
            lastUserCheckedOut.setText(getString(R.string.last_user_checkout_name) + deviceInfoGlobal.getLastCheckedOutBy());
        }
        if(deviceInfoGlobal.getLastCheckedOutDate() != null) {
            lastDateCheckedOut.setText(getString(R.string.detail_date_checkout_name) + DateUtil.getFormattedDate(deviceInfoGlobal.getLastCheckedOutDate()));
        }
        if(deviceInfoGlobal.isCheckedOut()){

            btnStatus.setText(getString(R.string.btn_checked_in_label));
        }else {
            btnStatus.setText(getString(R.string.btn_checked_out_label));
        }
    }


    private DeviceUpdateRequest getUpdateRequest(boolean isCheckout, String name){

        DeviceUpdateRequest deviceUpdateRequest = new DeviceUpdateRequest();
        if(isCheckout){
            deviceUpdateRequest.setLastCheckedOutBy(null);
            deviceUpdateRequest.setLastCheckedOutDate(null);
            deviceUpdateRequest.setCheckedOut(false);
        }else{
            deviceUpdateRequest.setLastCheckedOutBy(name);
            deviceUpdateRequest.setLastCheckedOutDate(DateUtil.getCurrentTime());
            deviceUpdateRequest.setCheckedOut(true);
        }

        return deviceUpdateRequest;
    }

    private void updateDevice(final DeviceUpdateRequest deviceUpdateRequest){

        disposable.add(deviceViewModel.updateDevice(String.valueOf(deviceInfoGlobal.getId()), deviceUpdateRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ResponseBody>() {
                    @Override
                    public void accept(ResponseBody responseBody) throws Exception {
                        refreshCurrentDevice();
                    }
                }));



    }

    private void refreshCurrentDevice(){

        disposable.add(deviceViewModel.getDevices()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<DeviceInfo>>() {
                    @Override
                    public void accept(List<DeviceInfo> deviceInfos) throws Exception {
                        if(deviceInfos.size() > 0){
                            for(DeviceInfo deviceInfo : deviceInfos){
                                if(deviceInfo.getId() == deviceInfoGlobal.getId()){
                                    deviceInfoGlobal = deviceInfo;
                                    fillInfo();
                                }
                            }
                        }
                    }
                }));
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
