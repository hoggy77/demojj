package com.demo.johnson.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.Toast;

import com.demo.johnson.R;
import com.demo.johnson.ui.adapter.DeviceAdapter;
import com.demo.johnson.model.DeviceInfo;
import com.demo.johnson.utils.DialogUtil;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class HomeActivity extends AppCompatActivity implements CustomItemTouchHelperCallback.TouchHelperInterface {

    private DeviceViewModel deviceViewModel;
    private CompositeDisposable disposable = new CompositeDisposable();
    private RecyclerView deviceList;
    private FloatingActionButton btnAddDevice;
    private DeviceAdapter adapter;
    private CustomItemTouchHelperCallback.TouchHelperInterface activityListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        init();
    }


    private void init(){
        deviceViewModel = ViewModelProviders.of(HomeActivity.this).get(DeviceViewModel.class);
        adapter = new DeviceAdapter(this);
        deviceList = findViewById(R.id.device_list);
        btnAddDevice = findViewById(R.id.btn_add_device);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HomeActivity.this, LinearLayoutManager.VERTICAL,false);
        deviceList.setLayoutManager(linearLayoutManager);
        deviceList.setAdapter(adapter);

        DividerItemDecoration divider = new DividerItemDecoration(HomeActivity.this, linearLayoutManager.getOrientation());
        deviceList.addItemDecoration(divider);

        btnAddDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(HomeActivity.this, AddDeviceActivity.class);
                startActivity(intent);
            }
        });

        ItemTouchHelper.Callback callback = new CustomItemTouchHelperCallback(adapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(deviceList);

    }

    @Override
    protected void onResume() {
        super.onResume();
        getDevices();
    }

    private void getDevices(){
        disposable.add(deviceViewModel.getDevices()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<DeviceInfo>>() {
                               @Override
                               public void accept(List<DeviceInfo> devices) throws Exception {
                                   if (devices.size() > 0) {
                                       adapter.updateData(devices);
                                   }
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                Toast.makeText(HomeActivity.this, getString(R.string.devices_generic_error) + throwable.getMessage(),Toast.LENGTH_LONG).show();
                            }
                        })
        );
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }

    @Override
    public boolean onItemMove(int startPosition, int endPosition) {
        return false;
    }

    @Override
    public void onItemDismiss(final int adapterPosition) {

        DialogUtil.showDialog(HomeActivity.this, DialogUtil.TYPE_INFO, getString(R.string.alert_delete_message), new DialogUtil.DialogClickListener() {
            @Override
            public void onClick(String name) {
                deviceViewModel.deleteDevice(String.valueOf(adapter.getItem(adapterPosition).getId()));
                getDevices();
            }
        });
    }
}
