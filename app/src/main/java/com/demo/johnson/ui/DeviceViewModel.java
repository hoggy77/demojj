package com.demo.johnson.ui;

import android.arch.lifecycle.ViewModel;

import com.demo.johnson.model.AddDeviceModel;
import com.demo.johnson.model.DeviceInfo;
import com.demo.johnson.model.DeviceUpdateRequest;
import com.demo.johnson.repositories.DeviceRepository;

import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class DeviceViewModel extends ViewModel {

    public Single<List<DeviceInfo>> getDevices(){
        return DeviceRepository.getInstance().getDeviceList();
    }

    public Single<ResponseBody> updateDevice(String deviceID, DeviceUpdateRequest deviceUpdateRequest){
        return DeviceRepository.getInstance().updatedevice(deviceID, deviceUpdateRequest);
    }

    public Single<AddDeviceModel> addDevice(AddDeviceModel addDeviceModel){
        return DeviceRepository.getInstance().addDevice(addDeviceModel);
    }

    public Call<ResponseBody> deleteDevice(String deviceID){
        return DeviceRepository.getInstance().deleteDevice(deviceID);
    }
}
