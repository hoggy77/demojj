package com.demo.johnson.storage;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {OfflineCached.class}, exportSchema = false, version = 1)
public abstract class OfflineDatabase extends RoomDatabase {

    private static OfflineDatabase sInstance;
    private static Context mContext;

    public static synchronized OfflineDatabase getInstance(Context context) {
        if (sInstance == null) {
            mContext = context;
            Callback rdc = new Callback() {
                public void onCreate(SupportSQLiteDatabase db) {

                }

                public void onOpen(SupportSQLiteDatabase db) {
                    //do something every time database is open
                }
            };

            sInstance = Room.databaseBuilder(mContext.getApplicationContext(),
                    OfflineDatabase.class, "offline_storage_db")
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .addCallback(rdc).build();
        }
        return sInstance;
    }

    public abstract OfflineDao agendaDao();


    public void clearInstance() {
        sInstance = null;
    }



}