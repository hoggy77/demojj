package com.demo.johnson.storage;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class OfflineCached {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    public int id;

    @ColumnInfo(name = "action")
    public String action;

    @ColumnInfo(name = "type")
    public String type;

    public OfflineCached(String action, String type) {
        this.action = action;
        this.type = type;
    }
}