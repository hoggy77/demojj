package com.demo.johnson.storage;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import io.reactivex.Single;

@Dao
public interface OfflineDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insertRequest(OfflineCached request);

    @Delete
    int deleteRequests(OfflineCached favourite);

    @Query("DELETE FROM OfflineCached")
    void nukeTable();

    @Query("SELECT * FROM  OfflineCached")
    Single<OfflineCached> loadRequests();
}
