package com.demo.johnson.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.demo.johnson.R;

public class DialogUtil {

    public static final String TYPE_INFO = "type_info";
    public static final String TYPE_UPDATE = "type_update";

    public static void showDialog(Context mContext , String type, String messageString, final DialogClickListener listener ){

        final View dialogView = LayoutInflater.from(mContext).inflate(R.layout.dialog_layout, null);
        TextView message = dialogView.findViewById(R.id.dialog_message);
        final EditText name = dialogView.findViewById(R.id.edit_user_name);
        LinearLayout containerEdit = dialogView.findViewById(R.id.containerEdit);


        switch (type){
            case TYPE_INFO:
                containerEdit.setVisibility(View.GONE);
                message.setText(messageString);

                break;

            case TYPE_UPDATE:
                message.setVisibility(View.GONE);
                break;

        }

        AlertDialog dialog = new AlertDialog.Builder(mContext)
                .setView(dialogView)
                .setCancelable(false)
                .setPositiveButton(R.string.dialog_positive_btn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        listener.onClick(name.getText().toString());
                    }
                })

                .setNegativeButton(R.string.dialog_negative_btn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();

        dialog.show();
    }

    public interface DialogClickListener {
        void onClick(String name);
    }
}
