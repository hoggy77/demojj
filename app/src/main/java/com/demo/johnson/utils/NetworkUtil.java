package com.demo.johnson.utils;

import java.net.InetAddress;

public class NetworkUtil {

    public boolean isInternetAvailable() {
        try {
            InetAddress address = InetAddress.getByName("google.com");
            return !address.equals("");

        } catch (Exception e) {
            return false;
        }
    }
}
