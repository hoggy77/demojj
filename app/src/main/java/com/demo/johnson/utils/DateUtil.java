package com.demo.johnson.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtil {

    public static String getFormattedDate(String originalDateString){
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
            Date convertedDate = dateFormat.parse(originalDateString);
            SimpleDateFormat finalDate = new SimpleDateFormat("M/dd/yyyy h:mm aa", Locale.ENGLISH);
            return finalDate.format(convertedDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getCurrentTime(){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

        return sdf.format(Calendar.getInstance().getTime());
    }


}
